#MAC0110 - MiniEP7
#Nome: Leonardo Bozzetto NUSP: 11833091


#Adicione função seno
function taylor_sin(n)
    if abs(n)>2π

        n = n%2π

    end
    if n<0
        n = 2π + n
    end

    sinal = true #Variável utilizada para
    soma = 0
    i = 1
    pot = 1
    while abs(i) >= 0.00000000001
        i = big((n^pot)/(fatorial(pot)))
        if sinal
            soma = soma + i
            sinal = false
        else
            soma = soma - i
            sinal = true
        end
        pot = pot + 2
    end
    return soma
end

function taylor_cos(n)
    if abs(n)>2π
        n = n%2π
    end
    if n<0
        n = 2π + n
    end
    sinal = true #Variável utilizada para
    soma = 0
    i = 1
    pot = 0
    while i >= 0.0000000000001

        if sinal
            soma = soma + i
            sinal = false
        else
            soma = soma - i
            sinal = true
        end
        pot = pot + 2
        i = big((n^pot)/(fatorial(pot)))
    end
    return soma
end

function taylor_tan(n)
    inv = false
    if abs(n)>2π
        n = n%2π
    end

    if n<0
        n = abs(n)
        inv = !inv
    end

    if n>π/2
        if n<3π/2
            n = n-π
        else
            n = -(2π-n)
        end
    end

    if n<0
        n = abs(n)
        inv = !inv
    end

    soma = 0
    parte = 1
    i= 1
    while parte >= 0.000000000000000000001
        expo1 = big(2^(2*i))
        expo2 = big(2^(2*i)-1)
        expo3 = big(n^(2*i-1))
         parte = big(expo1*expo2*bernoulli(i)*expo3)/fatorial(2i)
         soma = soma + parte
         i = i + 1

    end
    if inv
        return -soma
    else
        return soma
    end
end

function fatorial(t)
    fato = big(1)
    for i in 1:t
        fato = fato*i
    end
    return fato
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
        for m = 0 : n
            A[m + 1] = 1 // (m + 1)
            for j = m : -1 : 1
                A[j] = j * (A[j] - A[j + 1])
            end
        end
    return abs(A[1])
end

function check_sin(valor, n)
    t1 = false
    t2 = false

    if abs(valor - taylor_sin(n)) <= 0.001
        t1 = true
    end
    if abs(sin(n) - taylor_sin(n)) <= 0.001
        t2 = true
    end
    if t1 && t2
        return true
    else
        return false
    end
end

function check_cos(valor, n)
    t1 = false
    t2 = false
    if abs(valor - taylor_cos(n)) <= 0.001
        t1 = true
    end
    if abs(cos(n) - taylor_cos(n)) <= 0.001
        t2 = true
    end
    if t1 && t2
        return true
    else
        return false
    end

end

function check_tan(valor, n)
    t1 = false
    t2 = false

    if abs(valor - taylor_tan(n)) <= 0.001
        t1 = true
    end
    if abs(tan(n) - taylor_tan(n)) <= 0.001
        t2 = true
    end

    if t1 && t2
        return true
    else
        return false
    end
end
function test()
    imp = important_ang()
    lim = limite()
    if imp
        println("As funções funcionam para ângulos notaveis")
    end
    if lim
        println("As funções funcionam para casos limitrofes")
    end
end

function important_ang()
    if !check_sin(0,0)
        println("Falhou para a função seno de 0 = 0")
        return false
    end
    if !check_sin(1/2,π/6)
        println("Falhou para a função seno de π/6 = 0.5")
        return false
    end
    if !check_sin(0.7071,π/4)
        println("Falhou para a função seno de π/4 = √2/2")
        return false
    end
    if !check_sin(0.8660,π/3)
        println("Falhou para a função seno de π/3 = √3/2")
        return false
    end
    if !check_sin(1,π/2)
        println("Falhou para a função seno de π/2 = 1")
        return false
    end
    if !check_sin(0.8660,2π/3)
        println("Falhou para a função seno de 2π/3 = √3/2")
        return false
    end
    if !check_sin(0.7071,3π/4)
        println("Falhou para a função seno de 3π/4 = √2/2")
        return false
    end
    if !check_sin(1/2,5π/6)
        println("Falhou para a função seno de 5π/6 = 0.5")
        return false
    end
    if !check_sin(0,π)
        println("Falhou para a função seno de π = 0")
        return false
    end
    if !check_sin(-1/2,7π/6)
        println("Falhou para a função seno de 7π/6 = -0.5")
        return false
    end
    if !check_sin(-0.7071,5π/4)
        println("Falhou para a função seno de 5π/4 = -√2/2")
        return false
    end
    if !check_sin(-0.8660,4π/3)
        println("Falhou para a função seno de 4π/3 = -√3/2")
        return false
    end
    if !check_sin(-1,3π/2)
        println("Falhou para a função seno de 3π/2 = -1")
        return false
    end
    if !check_sin(-0.8660,5π/3)
        println("Falhou para a função seno de 5π/3 = -√3/2")
        return false
    end
    if !check_sin(-0.7071,7π/4)
        println("Falhou para a função seno de 7π/4 = -√2/2")
        return false
    end
    if !check_sin(-1/2,11π/6)
        println("Falhou para a função seno de 11π/6 = -0.5")
        return false
    end
    if !check_sin(0,2π)
        println("Falhou para a função seno de 2π = 0")
        return false
    end
    if !check_cos(1,0)
        println("Falhou para a função cosseno de 0 = 1")
        return false
    end
    if !check_cos(0.8660,π/6)
        println("Falhou para a função cosseno de π/6 = 0.5")
        return false
    end
    if !check_cos(0.7071,π/4)
        println("Falhou para a função cosseno de π/4 = √2/2")
        return false
    end
    if !check_cos(1/2,π/3)
        println("Falhou para a função cosseno de π/3 = √3/2")
        return false
    end
    if !check_cos(0,π/2)
        println("Falhou para a função cosseno de π/2 = 1")
        return false
    end
    if !check_cos(-1/2,2π/3)
        println("Falhou para a função cosseno de 2π/3 = √3/2")
        return false
    end
    if !check_cos(-0.7071,3π/4)
        println("Falhou para a função cosseno de 3π/4 = √2/2")
        return false
    end
    if !check_cos(-0.8660,5π/6)
        println("Falhou para a função cosseno de 5π/6 = 0.5")
        return false
    end
    if !check_cos(-1,π)
        println("Falhou para a função cosseno de π = 0")
        return false
    end
    if !check_cos(-0.8660,7π/6)
        println("Falhou para a função cosseno de 7π/6 = -0.5")
        return false
    end
    if !check_cos(-0.7071,5π/4)
        println("Falhou para a função cosseno de 5π/4 = -√2/2")
        return false
    end
    if !check_cos(-1/2,4π/3)
        println("Falhou para a função cosseno de 4π/3 = -√3/2")
        return false
    end
    if !check_cos(0,3π/2)
        println("Falhou para a função cosseno de 3π/2 = -1")
        return false
    end
    if !check_cos(1/2,5π/3)
        println("Falhou para a função cosseno de 5π/3 = -√3/2")
        return false
    end
    if !check_cos(0.7071,7π/4)
        println("Falhou para a função cosseno de 7π/4 = -√2/2")
        return false
    end
    if !check_cos(0.8660,11π/6)
        println("Falhou para a função cosseno de 11π/6 = -0.5")
        return false
    end
    if !check_cos(1,2π)
        println("Falhou para a função cosseno de 2π = 0")
        return false
    end

    if !check_tan(0,0)
        println("Falhou para a função tangente de 0 = 0")
        return false
    end
    if !check_tan(1.7320, π/3)
        println("Falhou para a função tangente de π/3 = √3/3")
        return false
    end
    if !check_tan(0.5773,π/6)
        println("Falhou para a função tangente de π/6 = 0.5773")
        return false
    end
    if !check_tan(1,π/4)
        println("Falhou para a função tangente de π/4 = 1")
        return false
    end
    if !check_tan(-1.7320, -π/3)
        println("Falhou para a função tangente de -π/3 = -√3/3")
        return false
    end
    if !check_tan(-0.5773,-π/6)
        println("Falhou para a função tangente de -π/6 = -0.5773")
        return false
    end
    if !check_tan(-1,-π/4)
        println("Falhou para a função tangente de -π/4 = -1")
        return false
    end


    return true
end

function limite()
    if !check_sin(-0.5,-π/6)
        println("Falhou para a função seno de -π/6 = -0,5")
        return false
    end
    if !check_sin(-0.8660,-π/3)
        println("Falhou para a função seno de -π/3 = -0,8660")
        return false
    end
    if !check_sin(-0.7071,-π/4)
        println("Falhou para a função seno de -π/4 = -0,7071")
        return false
    end
    if !check_sin(0.5,25π/6)
        println("Falhou para a função seno de 25π/6 = 0,5")
        return false
    end
    if !check_sin(0.7071,17π/4)
        println("Falhou para a função seno de 17π/4 = 0,7071")
        return false
    end
    if !check_sin(0.8660,13π/3)
        println("Falhou para a função seno de 13π/3 = 0,8660")
        return false
    end
    if !check_sin(0,12π)
        println("Falhou para a função seno de 12π = 0")
        return false
    end
    if !check_cos(0.8660,-π/6)
        println("Falhou para a função cosseno de -π/6 = -0,8660")
        return false
    end
    if !check_cos(0.5,-π/3)
        println("Falhou para a função cosseno de -π/3 = -0,5")
        return false
    end
    if !check_cos(0.7071,-π/4)
        println("Falhou para a função cosseno de -π/4 = -0,7071")
        return false
    end
    if !check_cos(0.8660,25π/6)
        println("Falhou para a função cosseno de 25π/6 = 0,5")
        return false
    end
    if !check_cos(0.7071,17π/4)
        println("Falhou para a função cosseno de 17π/4 = 0,7071")
        return false
    end
    if !check_cos(0.5,13π/3)
        println("Falhou para a função cosseno de 13π/3 = 0,8660")
        return false
    end
    if !check_cos(1,12π)
        println("Falhou para a função cosseno de 12π = 1")
        return false
    end
    if !check_tan(-0.5773,-π/6)
        println("Falhou para a função tangente de -π/6 = -0,5773")
        return false
    end
    if !check_tan(-1.7320,-π/3)
        println("Falhou para a função tangente de -π/3 = -1,7320")
        return false
    end
    if !check_tan(-1,-π/4)
        println("Falhou para a função tangente de -π/4 = -1")
        return false
    end
    if !check_tan(0.5773,25π/6)
        println("Falhou para a função tangente de 25π/6 = 0,5773")
        return false
    end
    if !check_tan(1,17π/4)
        println("Falhou para a função tangente de 17π/4 = 1")
        return false
    end
    if !check_tan(1.7320,13π/3)
        println("Falhou para a função tangente de 13π/3 = 1,7320")
        return false
    end
    if !check_tan(0,12π)
        println("Falhou para a função seno de 12π = 0")
        return false
    return true
    end
end

function main()
    test()
end

main()
